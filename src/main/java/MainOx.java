
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class MainOx {

    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);

        System.out.println("Welcome to OX Game");

        char[][] table = new char[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }

        boolean player1 = true;

        //เกมจบ
        boolean gameEnd = false;

        while (!gameEnd) {

            phototable(table);

            if (player1) {
                System.out.println("Turn O");
            } else {
                System.out.println("Turn X");
            }

            char turn = ' ';
            if (player1) {
                turn = 'O';
            } else {
                turn = 'X';
            }

            int row = 0;
            int col = 0;

            while (true) {

                System.out.println("Please input row, col :");
                row = kb.nextInt();
                col = kb.nextInt();

                row -= 1;
                col -= 1;

                if (row < 0 || row > 2 || col < 0 || col > 2) {
                    System.out.println(" error you row and col are out");
                } else if (table[row][col] != '-') {
                    System.out.println(" error mode a mone there");
                } else {
                    break;
                }

            }

            table[row][col] = turn;

            //เช็ค ผู้เล่นใครชนะ
            if (Chacktable(table) == 'O') {
                phototable(table);
                System.out.println(">>>O Win<<<");
                gameEnd = true;
            } else if (Chacktable(table) == 'X') {
                phototable(table);
                System.out.println(">>>X Win<<<");
                gameEnd = true;
            } else {
                if (Tablefull(table)) {
                    phototable(table);
                    System.out.println(">>>Always<<<");
                    gameEnd = true;
                } else {
                    //ยังไม่มีใครชนะ เล่นเกมต่อ
                    if (player1) {
                        player1 = false;
                    } else {
                        player1 = true;
                    }
                }

            }

        }
    }

    public static void phototable(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static char Chacktable(char[][] table) {
        //แถวชนะ
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == table[i][1] && table[i][1] == table[i][2] && table[i][2] != '-') {
                return table[i][0];
            }
        }

        //คอลัมชนะ
        for (int j = 0; j < 3; j++) {
            if (table[0][j] == table[1][j] && table[1][j] == table[2][j] && table[0][j] != '-') {
                return table[0][j];
            }
        }

        //เส้นทะแยงชนะ
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
            return table[0][0];
        }

        if (table[2][0] == table[1][1] && table[1][1] == table[0][2] && table[2][0] != '-') {
            return table[2][0];
        }

        //ไม่มีใครชนะ
        return ' ';
    }

    //เช็ค ตารางเต็ม or เต็ม
    public static boolean Tablefull(char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
